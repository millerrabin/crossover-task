How to install
1. Install latest version of Node JS from https://nodejs.org
2. Change directory to project/Code folder
3. launch "npm install" to download project dependencies
4. launch "npm install mocha -g" if you want to launch tests
5. launch "npm test" to launch unit tests
6. launch "npm start" to start web server
7. Application will be available at https://localhost:8080