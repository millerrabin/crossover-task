const queue = require('./queue.js');
const Mongo = require('mongodb');

function getIp(request) {
    let str = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
    let rip = new RegExp("\\[([0-9a-f:]+)\\]:([0-9]{1,5})");
    let res = str.match(rip);
    if (res != null) return res[1];

    let ripv4 = new RegExp("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}):([0-9]{1,5})");
    res = str.match(ripv4);
    if (res != null) return res[1];
    return str;
}

exports.add = function(data) {
    return new Promise((resolve, reject) => {
        if (global.database == null) return reject({ message: 'database is not initialized'});
        global.database.collection('crossover').insertOne(data, (err, result) => {
            if (err != null) return reject(err);
            data._id =  result.insertedId;
            queue.add(data);
            return resolve({ id: result.insertedId });
        });
    });
};

exports.update = function(idText, data) {
    return new Promise((resolve, reject) => {
        if (global.database == null) return reject({ message: 'database is not initialized'});
        let id = new Mongo.ObjectId(idText);
        global.database.collection('crossover').replaceOne({ _id: id }, data, (err, results) => {
            if (err != null) return reject(err);
            if (results.matchedCount == 0) return reject({ message: 'No data updated' });
            data._id =  idText;
            queue.update(data);
            return resolve({ id: id });
        });
    });
};

exports.delete = function(idText) {
    return new Promise((resolve, reject) => {
        if (global.database == null) return reject({ message: 'database is not initialized'});
        let id = new Mongo.ObjectId(idText);
        global.database.collection('crossover').findOneAndDelete({ _id: id }, (err, results) => {
            if (err != null) return reject(err);
            if (results.matchedCount == 0) return reject({ message: 'No data deleted' });
            queue.delete({ _id: idText });
            return resolve();
        });
    });
};

exports.get = function(query) {
    return new Promise((resolve, reject) => {
        if (global.database == null) return reject({ message: 'database is not initialized'});
        global.database.collection('crossover').find(query, (err, data) => {
            if (err != null) return reject(err);
            return resolve(data.toArray());
        });
    });
};

exports.addController = function(application, controllerName) {
    application.post('/' + controllerName, function (request, response) {
        let body = request.body;
        body.ip = getIp(request);
        body.time = new Date();
        return exports.add(body).then((data) => {
            response.json(data);
        }).catch((err) => {
            response.status(400);
            response.json(err);
        });
    });

    application.put('/' + controllerName, function (request, response) {
        let body = request.body;
        body.ip = getIp(request);
        body.time = new Date();
        if (body.id == null) {
            response.status = 400;
            response.json({ message: 'no record id'});
            return;
        }
        let id = body.id;
        delete body.id;
        return exports.update(id, body).then((data) => {
            response.json(data);
        }).catch((err) => {
            response.status(400);
            response.json(err);
        });
    });

    application.delete('/' + controllerName, function (request, response) {
        let body = request.body;
        body.ip = getIp(request);
        body.time = new Date();
        if (body.id == null) {
            response.status = 400;
            response.json({ message: 'no record id'});
            return;
        }
        let id = body.id;
        return exports.delete(id).then((data) => {
            response.json(data);
        }).catch((err) => {
            response.status(400);
            response.json(err);
        });
    });
};
