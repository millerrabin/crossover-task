const MongoClient = require('mongodb').MongoClient;

exports.connect = function() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(global.mongoPath, function(err, db) {
            if (err != null) return reject(err);
            console.log("Connected successfully to server");
            return resolve(db);
        });
    });
};

