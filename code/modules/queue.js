const io = require('socket.io');

const donors = require('./donors.js');
let ioServer = null;
global.timeout = (global.timeout == null) ? 500 : global.timeout;

let queue = [

];

exports.add = function(data) {
    queue.push({
        operation: 'add',
        data: data
    });
};

exports.update = function(data) {
    queue.push({
        operation: 'update',
        data: data
    });
};

exports.delete = function(data) {
    queue.push({
        operation: 'delete',
        data: data
    });
};

exports.create = function (server) {
    return Promise.resolve().then(() => {
        ioServer = io.listen(server);
        ioServer.sockets.on('connection', function (socket) {
            donors.get({}).then((data) => {
                socket.json.send(data);
            });

            socket.on('message', function (msg) {

            });
            socket.on('disconnect', function() {

            });
        });
    });
};

function flush() {
    if (ioServer == null) return;
    if (queue.length == 0) return;
    let keys = Object.keys(ioServer.sockets.connected);
    if (keys.length == 0) return;
    for (let i = 0; i < keys.length; i++) {
        let socket = ioServer.sockets.connected[keys[i]];
        socket.json.send(queue);
    }
    queue.length = 0;
}

setInterval(flush, global.timeout);