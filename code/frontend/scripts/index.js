require([
    "/scripts/modules/geoSource.js",
    "/scripts/modules/donors.js",
    "esri/tasks/Locator",
    "esri/Map",
    "esri/views/MapView",
    "esri/widgets/Search",
    "dojo/request/xhr",
    "dojo/domReady!"
], function(geoSource, donors, Locator, Map, MapView, Search, xhr) {
    let locatorTask = new Locator({
        url: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer"
    });

    let map = new Map({
        basemap: "streets"
    });

    let view = new MapView({
        container: "viewDiv",
        map: map,
        center: [37.617635, 55.755814],
        scale: 24000
    });

    let search = new Search({
        view: view
    });

    view.ui.add(search, "top-right");

    function showForm(view, address, point, item) {
        view.popup.content = donors.addDialog({
            view: view,
            address: address,
            point: { latitude: point.latitude, longitude: point.longitude },
            data: item
        });
        view.popup.open({
            title: address,
            location: point
        });
    }

    function reverseGeoCoder(view, event) {
        return locatorTask.locationToAddress(event.mapPoint).then(function(response) {
            showForm(view, response.address.Match_addr, event.mapPoint);
        }).otherwise(function(err) {
            showForm(view, 'No address', event.mapPoint);
        });
    }

    function navigate() {
        if (window.navigator.geolocation)
            window.navigator.geolocation.getCurrentPosition(function (position) {
                view.goTo([position.coords.longitude, position.coords.latitude]);
            }, function (err) {
                console.log(err);
                console.log('try to navigate by ipinfo');
                xhr("https://ipinfo.io/json", {
                    handleAs: "json",
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (ipinfo) {
                    let coords = ipinfo.loc.split(',');
                    view.goTo([parseFloat(coords[1]), parseFloat(coords[0])]);
                }).otherwise(function (err) {
                    console.log(err);
                });
            });
    }

    view.on("click", function(event) {
        let screenPoint = {
            x: event.x,
            y: event.y
        };
        return view.hitTest(screenPoint).then(function(response){
            if (response.results.length == 0) {
                return reverseGeoCoder(view, event);
            } else {
                let id = donors.getCurrentId();
                let item = response.results[0].graphic;
                if (item.attributes.ObjectID == id) {
                    showForm(view, item.attributes.location, item.geometry, item);
                    event.stopPropagation();
                }
            }
        });
    });

    view.then(function() {
        navigate();
        map.add(geoSource.layer);
        setTimeout(function () {
            let id = donors.getCurrentId();
            if (id == null) return;
            let item = geoSource.find({ attributes: { ObjectID: id }});
            if (item == null) return;
            showForm(view, item.attributes.location, item.geometry, item);
        }, 500);
    });
});