define([], function () {
    function validName(data) {
        let text = data.node.value;
        let validation = data.validation;
        validation.innerHTML = '';
        if (text == '') {
            validation.innerHTML = 'The field is empty';
            return false;
        }
        return true;
    }

    function validEmail(data) {
        let text = data.node.value;
        let validation = data.validation;
        validation.innerHTML = '';
        if (text == '') {
            validation.innerHTML = 'The field is empty';
            return false;
        }
        let reg = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
        let res = reg.exec(text);
        if (res != null) return true;
        validation.innerHTML = 'The field is incorrect';
        return false;
    }

    function validPhone(data) {
        let text = data.node.value;
        let validation = data.validation;
        validation.innerHTML = '';
        if (text == '') {
            validation.innerHTML = 'The field is empty';
            return false;
        }
        text = text.replace(/\s+/g, '');
        text = text.replace(/^\+/, '00');
        let reg = /^00[0-9]+$/i;
        let res = reg.exec(text);
        if (res != null) return true;
        validation.innerHTML = 'The phone must be in (+xx xxx xxxx xxx) or (00xx xxx xxxx xxx) format';
        return false;
    }

    function validSelect(data) {
        let text = data.node.value;
        let validation = data.validation;
        validation.innerHTML = '';
        if (text == '') {
            validation.innerHTML = 'Please select';
            return false;
        }
        return true;
    }
    return {
        name: validName,
        email: validEmail,
        phone: validPhone,
        select: validSelect
    }
});


