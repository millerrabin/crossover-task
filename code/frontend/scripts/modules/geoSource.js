define(["esri/renderers/SimpleRenderer",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/geometry/Point",
        "esri/layers/FeatureLayer"],
function (SimpleRenderer, SimpleMarkerSymbol, Point, FeatureLayer) {
    let returnData = {};
    let bloodTable = {
        1: 'O(I)',
        2: 'A(II)',
        3: 'B(III)',
        4: 'AB(IV)'
    };

    let socket = window.io.connect('/', {secure: true});

    socket.on('connect', function () {
        socket.on('message', function (geoData) {
            addGraphics(geoData);
        });
    });

    let fields = [{
        name: "ObjectID",
        alias: "ObjectID",
        type: "oid"
    }, {
        name: "donor",
        alias: "donor",
        type: "string"
    }, {
        name: "blood",
        alias: "blood",
        type: "string"
    }, {
        name: "contactNumber",
        alias: "contactNumber",
        type: "string"
    }, {
        name: "email",
        alias: "email",
        type: "string"
    }, {
        name: "location",
        alias: "location",
        type: "string"
    }, {
        name: "time",
        alias: "time",
        type: "date"
    }];

    let template = {
        title: "{donor} {blood}",
        content: [{
            type: "fields",
            fieldInfos: [{
                fieldName: "donor",
                label: "Donor",
                visible: true
            }, {
                fieldName: "blood",
                label: "Blood group",
                visible: true
            }, {
                fieldName: 'contactNumber',
                label: "Contact number",
                visible: true
            }, {
                fieldName: 'email',
                label: "Email",
                visible: true
            }, {
                fieldName: "location",
                label: "Location",
                visible: true
            }, {
                fieldName: "time",
                label: "Date and time",
                visible: true
            }]
        }],
        fieldInfos: [{
            fieldName: "time",
            format: {
                dateFormat: "short-date-short-time"
            }
        }]
    };

    let renderer = new SimpleRenderer({
        symbol: new SimpleMarkerSymbol({
            style: "circle",
            size: 10,
            color: [210, 0, 0, 1]
        })
    });

    returnData.layer = new FeatureLayer({
        source: [],
        fields: fields,
        objectIdField: "ObjectID",
        renderer: renderer,
        spatialReference: {
            wkid: 4326
        },
        geometryType: "point",
        popupTemplate: template
    });

    function createItem(feature) {
        let item = {
            geometry: new Point({
                x: feature.geometry.longitude,
                y: feature.geometry.latitude
            }),
            attributes: {
                ObjectID: feature._id,
                location: feature.address,
                donorData: {
                    firstName: feature.firstName,
                    lastName: feature.lastName,
                    contactNumber: feature.phone,
                    email: feature.email,
                    blood: {
                        group: feature.blood,
                        rhesus: feature.rhesus
                    }
                },
                time: new Date(feature.time).getTime()
            }
        };
        item.attributes.donor = item.attributes.donorData.firstName + ' ' + item.attributes.donorData.lastName;
        item.attributes.blood = bloodTable[item.attributes.donorData.blood.group] + item.attributes.donorData.blood.rhesus;
        item.attributes.email = item.attributes.donorData.email;
        item.attributes.contactNumber = item.attributes.donorData.contactNumber;
        return item;
    }


    function add(feature) {
        let item = createItem(feature);
        returnData.layer.source.add(item);
    }

    returnData.find = function(item) {
        for (let i = 0; i < returnData.layer.source.items.length; i++) {
            let data = returnData.layer.source.items[i];
            if (data.attributes.ObjectID == item.attributes.ObjectID) return data;
        }
        return null;
    };


    function update(feature) {
        let item = createItem(feature);
        let spoint = returnData.find(item);
        if (spoint == null) return;
        Object.assign(spoint, item);
    }

    function remove(feature) {
        let item = { attributes: { ObjectID: feature._id }};
        let spoint = returnData.find(item);
        if (spoint == null) return;
        returnData.layer.source.remove(spoint);
    }

    function addGraphics(geoData) {
        for (let i = 0; i< geoData.length; i++) {
            let feature = geoData[i];
            if (feature.operation == null) {
                add(feature);
                continue;
            }
            if (feature.operation == 'add') {
                add(feature.data);
                continue;
            }
            if (feature.operation == 'update') {
                update(feature.data);
                continue;
            }
            if (feature.operation == 'delete') {
                remove(feature.data);
            }
        }
    }

    return returnData;
});
