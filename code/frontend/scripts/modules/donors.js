define(['dojo/dom-construct',
        'dojo/on',
        "dojo/request/xhr",
        "/scripts/modules/validation.js",],
function (domConstruct, on, request, validation) {

    function getId() {
        let oid = window.location.search.split('?');
        if (oid[1] != null) return oid[1];
        return null;
    }

    let currentId = getId();

    function createTitle(parent, title) {
        let titleNode = domConstruct.create('h2', { innerHTML: title });
        domConstruct.place(titleNode, parent);
        return titleNode;
    }

    function createInput(parent, options) {
        let node = domConstruct.create('label', { innerHTML: '<span class="fieldName">' + options.title + '</span>'});
        let inputNode = domConstruct.create('input', { type: 'text', maxLength: 32});
        let validationNode = domConstruct.create('span', { class: 'error'});
        domConstruct.place(inputNode, node);
        domConstruct.place(validationNode, node);
        domConstruct.place(node, parent);
        return { node: inputNode, validation: validationNode };
    }

    function createButton(parent, options) {
        let container = domConstruct.create('div', { class: 'buttonContainer' });
        let node = domConstruct.create('button', { innerHTML: options.title, type: 'button' });
        let messageNode = domConstruct.create('span', { class: 'message'});
        domConstruct.place(container, parent);
        domConstruct.place(node, container);
        domConstruct.place(messageNode, container);
        on(node, 'click', options.click);
        return { node: node, validation: messageNode };
    }

    function createMessage(parent) {
        let node = domConstruct.create('div', { class: 'message' });
        domConstruct.place(node, parent);
        return { node: node };
    }

    function setUpdateMode(data) {
        data.submit.node.style.display = 'none';
        data.update.node.style.display = 'block';
        data.delete.node.style.display = 'block';
    }

    function createSelect(parent, data) {
        let opArr = [];
        for (let i = 0; i < data.options.length; i++) {
            let item = data.options[i];
            opArr.push('<option value="' + item.value + '">' + item.text + '</option>');
        }
        let node = domConstruct.create('label', { innerHTML: '<span class="fieldName">' + data.title + '</span>'});
        let selectNode = domConstruct.create('select', { innerHTML: opArr.join('') });
        let validationNode = domConstruct.create('span', { class: 'error'});
        domConstruct.place(selectNode, node);
        domConstruct.place(validationNode, node);
        domConstruct.place(node, parent);
        return { node: selectNode, validation: validationNode };
    }

    function createNode(fields) {
        let node = domConstruct.create('form', { class: 'addDonor'});
        let data = Object.assign(fields);
        createTitle(node, 'Please submit the form');
        data.firstName = createInput(node, { title: 'First name'});
        data.lastName = createInput(node, { title: 'Last name'});
        data.email = createInput(node, { title: 'Email'});
        data.phone = createInput(node, { title: 'Contact number'});
        data.blood = createSelect(node, { title: 'Blood group',
            options: [
                { default: true, value: '', text: 'Group of blood'},
                { text: 'O(I)', value: 1 },
                { text: 'A(II)', value: 2 },
                { text: 'B(III)', value: 3 },
                { text: 'AB(IV)', value: 4 }

        ]});
        data.rhesus = createSelect(node, { title: 'Rh factor',
            options: [
                { default: true, value: '', text: 'Rh factor'},
                { text: '+', value: '+' },
                { text: '-', value: '-' }

            ]});
        data.submit = createButton(node, { title: 'Save data', click: saveData });
        data.update = createButton(node, { title: 'Update', click: updateData });
        data.delete = createButton(node, { title: 'Delete', click: deleteData });
        data.message = createMessage(node);
        data.submit.node.formData = data;
        data.update.node.formData = data;
        data.delete.node.formData = data;
        data.update.node.style.display = 'none';
        data.delete.node.style.display = 'none';
        if (fields.data == null) return node;
        data.firstName.node.value = fields.data.attributes.donorData.firstName;
        data.lastName.node.value = fields.data.attributes.donorData.lastName;
        data.email.node.value = fields.data.attributes.donorData.email;
        data.phone.node.value = fields.data.attributes.donorData.contactNumber;
        data.blood.node.value = fields.data.attributes.donorData.blood.group;
        data.rhesus.node.value = fields.data.attributes.donorData.blood.rhesus;
        data.id = fields.data.attributes.ObjectID;
        setUpdateMode(data);
        return node;
    }

    function updateData(event) {
        event.stopPropagation();
        event.preventDefault();
        let data = event.target.formData;
        if (data.id == null) return;
        let fres = validation.name(data.firstName);
        let lres = validation.name(data.lastName);
        let mres = validation.email(data.email);
        let pres = validation.phone(data.phone);
        let bres = validation.select(data.blood);
        let rres = validation.select(data.rhesus);
        if (!fres || !lres || !mres || !pres || !bres || !rres) return;
        let obj = {
            id: data.id,
            firstName: data.firstName.node.value,
            lastName: data.lastName.node.value,
            email: data.email.node.value,
            phone: data.phone.node.value,
            blood: data.blood.node.value,
            rhesus: data.rhesus.node.value,
            geometry: data.point,
            address: data.address
        };

        request('/api/donors', {
            method: 'PUT',
            handleAs: "json",
            headers: { 'Content-Type': 'application/json' },
            data: JSON.stringify(obj)}).then(function (response) {
                data.message.node.innerHTML = '<p>Your Data has been updated.</p>' +
                    '<p>Your Private link is</p>' +
                    '<p><a href="/index.html?' + response.id + '">https://crossover.znayu.pro/index.html?' + response.id + '</a></p>' +
                    '<p>Please, save it anywhere and use to edit or delete your data</p>';
        }).otherwise(function (err) {
            console.log(err);
        });
    }

    function deleteData(event) {
        event.stopPropagation();
        event.preventDefault();
        let data = event.target.formData;
        if (data.id == null) return;
        let obj = { id: data.id };
        request('/api/donors', {
            method: 'DELETE',
            handleAs: "json",
            headers: { 'Content-Type': 'application/json' },
            data: JSON.stringify(obj)}).then(function () {
                data.message.node.innerHTML = '';
                data.view.popup.close();
        }).otherwise(function (err) {
            console.log(err);
        });
    }

    function saveData(event) {
        event.stopPropagation();
        event.preventDefault();
        let data = event.target.formData;
        let fres = validation.name(data.firstName);
        let lres = validation.name(data.lastName);
        let mres = validation.email(data.email);
        let pres = validation.phone(data.phone);
        let bres = validation.select(data.blood);
        let rres = validation.select(data.rhesus);
        if (!fres || !lres || !mres || !pres || !bres || !rres) return;
        let obj = {
            firstName: data.firstName.node.value,
            lastName: data.lastName.node.value,
            email: data.email.node.value,
            phone: data.phone.node.value,
            blood: data.blood.node.value,
            rhesus: data.rhesus.node.value,
            geometry: data.point,
            address: data.address
        };

        request('/api/donors', {
            method: 'POST',
            handleAs: "json",
            headers: { 'Content-Type': 'application/json' },
            data: JSON.stringify(obj)}).then(function (response) {
                data.message.node.innerHTML = '<p>Your Data hass been saved.</p>' +
                    '<p>Your Private link is</p>' +
                    '<p><a href="/index.html?' + response.id + '">https://crossover.znay.pro/index.html?' + response.id + '</a></p>' +
                    '<p>Please, save it anywhere and use to edit or delete your data</p>';
                setUpdateMode(data);
                currentId = response.id;
                data.id = response.id;
        }).otherwise(function (err) {
            console.log(err);
        });
    }

    return {
        addDialog: createNode,
        getCurrentId: getId
    }
});

