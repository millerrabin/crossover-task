const assert = require('assert');
global.mongoPath = 'mongodb://crossover:123456@node3.raintech.su:27017/crossover';

const database = require('../modules/database.js');
const donors = require('../modules/donors.js');

describe('Donors', function () {
    describe('Connect to database', function () {
        it('Connect to database',  () => {
            return database.connect().then((db) => {
                global.database = db;
                return Promise.resolve();
            });
        });
    });

    describe('Data manipulate', function () {
        let recordId = null;
        let testObj = {
            firstName: 'Test',
            lastName: 'Test',
            email: 'test@mail.ru',
            phone: '+71234567890',
            blood: '1',
            rhesus: '+',
            geometry: {
                latitude: 0,
                longitude: 0
            },
            address: 'Not specified'
        };

        it('get donors data', () => {
            return donors.get();
        });


        it('insert donor', () => {
            return donors.add(testObj).then((result) => {
                recordId = result.id;
            });
        });

        it('update donor', () => {
            assert.notEqual(recordId, null, 'Record mut not be null');
            testObj.firstName = 'test2';
            return donors.update(recordId, testObj);
        });

        it('delete donor', () => {
            assert.notEqual(recordId, null, 'Record mut not be null');
            return donors.delete(recordId);
        });
    });

});

