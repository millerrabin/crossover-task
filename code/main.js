const express = require('express');
const app = express();
const path = require('path');
const https = require('https');
const fs = require('fs');

global.mongoPath = 'mongodb://crossover:123456@node3.raintech.su:27017/crossover';

const database = require('./modules/database.js');
const donors = require('./modules/donors.js');
const queue = require('./modules/queue.js');

const bodyParser = require('body-parser');

global.port = process.env.PORT || 8080;
global.database = null;

app.use(bodyParser.json());
app.use('/', express.static(path.join(__dirname, 'frontend')));
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));

donors.addController(app, 'api/donors');

function createServer(application, port) {
    return new Promise((resolve, reject) => {
        let options = {
            key: fs.readFileSync(path.join(__dirname, 'certs/private.key')),
            cert: fs.readFileSync(path.join(__dirname, 'certs/cert.crt'))
        };

        let server = https.createServer(options, application);
        server.listen(port, (err) => {
            if (err != null) return reject(err);
            console.log('Server listening on port ' + port);
            return resolve({ server: server });
        });
    });
}

process.on('unhandledRejection', function(reason, p) {
    console.log("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
});

return createServer(app, global.port).then((data) => {
    return queue.create(data.server).then(() => {
        return database.connectMongoServer().then((db) => {
            global.database = db;
            return Promise.resolve({ database: db });
        });
    });
});